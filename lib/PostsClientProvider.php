<?php

namespace EnsiProject\PostsClient;

class PostsClientProvider
{
    /** @var string[] */
    public static $apis = ['\EnsiProject\PostsClient\Api\PostsApi', '\EnsiProject\PostsClient\Api\VotesApi'];

    /** @var string[] */
    public static $dtos = [
        '\EnsiProject\PostsClient\Dto\TagFillableProperties',
        '\EnsiProject\PostsClient\Dto\SearchPostsResponseMeta',
        '\EnsiProject\PostsClient\Dto\VoteFillableProperties',
        '\EnsiProject\PostsClient\Dto\PostFillableProperties',
        '\EnsiProject\PostsClient\Dto\SearchVotesRequest',
        '\EnsiProject\PostsClient\Dto\ResponseBodyOffsetPagination',
        '\EnsiProject\PostsClient\Dto\PaginationTypeEnum',
        '\EnsiProject\PostsClient\Dto\ErrorResponse2',
        '\EnsiProject\PostsClient\Dto\SearchPostsResponse',
        '\EnsiProject\PostsClient\Dto\PostResponse',
        '\EnsiProject\PostsClient\Dto\RequestBodyOffsetPagination',
        '\EnsiProject\PostsClient\Dto\PatchVoteRequest',
        '\EnsiProject\PostsClient\Dto\ReplacePostRequest',
        '\EnsiProject\PostsClient\Dto\CreateVoteRequest',
        '\EnsiProject\PostsClient\Dto\ErrorResponse',
        '\EnsiProject\PostsClient\Dto\SearchVotesResponse',
        '\EnsiProject\PostsClient\Dto\PostReadonlyProperties',
        '\EnsiProject\PostsClient\Dto\PatchPostRequest',
        '\EnsiProject\PostsClient\Dto\ResponseBodyPagination',
        '\EnsiProject\PostsClient\Dto\PostIncludes',
        '\EnsiProject\PostsClient\Dto\Tag',
        '\EnsiProject\PostsClient\Dto\HubReadonlyProperties',
        '\EnsiProject\PostsClient\Dto\ModelInterface',
        '\EnsiProject\PostsClient\Dto\PaginationTypeOffsetEnum',
        '\EnsiProject\PostsClient\Dto\EmptyDataResponse',
        '\EnsiProject\PostsClient\Dto\RequestBodyPagination',
        '\EnsiProject\PostsClient\Dto\Post',
        '\EnsiProject\PostsClient\Dto\ReplaceVoteRequest',
        '\EnsiProject\PostsClient\Dto\VotesValueEnum',
        '\EnsiProject\PostsClient\Dto\SearchPostsRequest',
        '\EnsiProject\PostsClient\Dto\VotesVoteEnum',
        '\EnsiProject\PostsClient\Dto\VoteReadonlyProperties',
        '\EnsiProject\PostsClient\Dto\Hub',
        '\EnsiProject\PostsClient\Dto\HubFillableProperties',
        '\EnsiProject\PostsClient\Dto\TagReadonlyProperties',
        '\EnsiProject\PostsClient\Dto\Vote',
        '\EnsiProject\PostsClient\Dto\CreatePostRequest',
        '\EnsiProject\PostsClient\Dto\Error',
        '\EnsiProject\PostsClient\Dto\PaginationTypeCursorEnum',
        '\EnsiProject\PostsClient\Dto\RequestBodyCursorPagination',
        '\EnsiProject\PostsClient\Dto\VoteResponse',
        '\EnsiProject\PostsClient\Dto\ResponseBodyCursorPagination',
    ];

    /** @var string */
    public static $configuration = '\EnsiProject\PostsClient\Configuration';
}
