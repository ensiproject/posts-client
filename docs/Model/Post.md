# # Post

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**title** | **string** | Заголовок поста | [optional] 
**body** | **string** | Содержимое поста | [optional] 
**rating** | **int** | Рейтинг поста | [optional] [default to 0]
**user_id** | **int** | Id пользователя | [optional] 
**hubs** | [**\EnsiProject\PostsClient\Dto\Hub[]**](Hub.md) |  | [optional] 
**tags** | [**\EnsiProject\PostsClient\Dto\Tag[]**](Tag.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


