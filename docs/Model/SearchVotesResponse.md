# # SearchVotesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\EnsiProject\PostsClient\Dto\Vote[]**](Vote.md) |  | 
**meta** | [**\EnsiProject\PostsClient\Dto\SearchPostsResponseMeta**](SearchPostsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


