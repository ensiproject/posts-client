# # PostIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tags** | [**\EnsiProject\PostsClient\Dto\Tag[]**](Tag.md) |  | [optional] 
**hubs** | [**\EnsiProject\PostsClient\Dto\Hub[]**](Hub.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


