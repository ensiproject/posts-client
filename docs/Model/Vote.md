# # Vote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор голоса | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**post_id** | **int** | Id поста | [optional] 
**user_id** | **int** | Id пользователя | [optional] 
**vote** | [**\EnsiProject\PostsClient\Dto\VotesValueEnum**](VotesValueEnum.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


