# # ReplacePostRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Заголовок поста | [optional] 
**body** | **string** | Содержимое поста | [optional] 
**rating** | **int** | Рейтинг поста | [optional] [default to 0]
**user_id** | **int** | Id пользователя | [optional] 
**hubs** | **int[]** |  | [optional] 
**tags** | **int[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


