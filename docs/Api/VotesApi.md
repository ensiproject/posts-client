# EnsiProject\PostsClient\VotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVote**](VotesApi.md#createVote) | **POST** /votes | Запрос для учета нового голоса для поста
[**deleteVote**](VotesApi.md#deleteVote) | **DELETE** /votes/{id} | Запрос на удаление голоса
[**getVotes**](VotesApi.md#getVotes) | **GET** /votes/{id} | Запрос на получение голосов поста
[**patchVote**](VotesApi.md#patchVote) | **PATCH** /votes/{id} | Запрос на обновление отдельных полей голоса
[**replaceVote**](VotesApi.md#replaceVote) | **PUT** /votes/{id} | Запрос на изменение голоса
[**searchVotes**](VotesApi.md#searchVotes) | **POST** /votes:search | Поиск голосов, удовлетворяющих фильтру



## createVote

> \EnsiProject\PostsClient\Dto\VoteResponse createVote($create_vote_request)

Запрос для учета нового голоса для поста

Запрос для учета нового голоса для поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_vote_request = new \EnsiProject\PostsClient\Dto\CreateVoteRequest(); // \EnsiProject\PostsClient\Dto\CreateVoteRequest | 

try {
    $result = $apiInstance->createVote($create_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_vote_request** | [**\EnsiProject\PostsClient\Dto\CreateVoteRequest**](../Model/CreateVoteRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \EnsiProject\PostsClient\Dto\EmptyDataResponse deleteVote($id)

Запрос на удаление голоса

Запрос на удаление голоса

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\EnsiProject\PostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getVotes

> \EnsiProject\PostsClient\Dto\VoteResponse getVotes($id, $include)

Запрос на получение голосов поста

Запрос на получение голосов поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getVotes($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->getVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\EnsiProject\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchVote

> \EnsiProject\PostsClient\Dto\VoteResponse patchVote($id, $patch_vote_request)

Запрос на обновление отдельных полей голоса

Запрос на обновление отдельных полей голоса

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_vote_request = new \EnsiProject\PostsClient\Dto\PatchVoteRequest(); // \EnsiProject\PostsClient\Dto\PatchVoteRequest | 

try {
    $result = $apiInstance->patchVote($id, $patch_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->patchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_vote_request** | [**\EnsiProject\PostsClient\Dto\PatchVoteRequest**](../Model/PatchVoteRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceVote

> \EnsiProject\PostsClient\Dto\VoteResponse replaceVote($id, $replace_vote_request)

Запрос на изменение голоса

Запрос на изменение голоса

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_vote_request = new \EnsiProject\PostsClient\Dto\ReplaceVoteRequest(); // \EnsiProject\PostsClient\Dto\ReplaceVoteRequest | 

try {
    $result = $apiInstance->replaceVote($id, $replace_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->replaceVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_vote_request** | [**\EnsiProject\PostsClient\Dto\ReplaceVoteRequest**](../Model/ReplaceVoteRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVotes

> \EnsiProject\PostsClient\Dto\SearchVotesResponse searchVotes($search_votes_request)

Поиск голосов, удовлетворяющих фильтру

Поиск голосов, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_votes_request = new \EnsiProject\PostsClient\Dto\SearchVotesRequest(); // \EnsiProject\PostsClient\Dto\SearchVotesRequest | 

try {
    $result = $apiInstance->searchVotes($search_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_votes_request** | [**\EnsiProject\PostsClient\Dto\SearchVotesRequest**](../Model/SearchVotesRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\SearchVotesResponse**](../Model/SearchVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

