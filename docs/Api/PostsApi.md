# EnsiProject\PostsClient\PostsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostsApi.md#createPost) | **POST** /posts | Запрос на создание нового поста
[**deletePost**](PostsApi.md#deletePost) | **DELETE** /posts/{id} | Запрос на удаление поста
[**getPosts**](PostsApi.md#getPosts) | **GET** /posts/{id} | Запрос на получение поста
[**patchPost**](PostsApi.md#patchPost) | **PATCH** /posts/{id} | Запрос на обновление отдельных полей поста
[**replacePost**](PostsApi.md#replacePost) | **PUT** /posts/{id} | Запрос на обновление поста
[**searchPosts**](PostsApi.md#searchPosts) | **POST** /posts:search | Поиск постов, удовлетворяющих фильтру



## createPost

> \EnsiProject\PostsClient\Dto\PostResponse createPost($create_post_request)

Запрос на создание нового поста

Запрос на создание нового поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_post_request = new \EnsiProject\PostsClient\Dto\CreatePostRequest(); // \EnsiProject\PostsClient\Dto\CreatePostRequest | 

try {
    $result = $apiInstance->createPost($create_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_post_request** | [**\EnsiProject\PostsClient\Dto\CreatePostRequest**](../Model/CreatePostRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \EnsiProject\PostsClient\Dto\EmptyDataResponse deletePost($id)

Запрос на удаление поста

Запрос на удаление поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\EnsiProject\PostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPosts

> \EnsiProject\PostsClient\Dto\PostResponse getPosts($id, $include)

Запрос на получение поста

Запрос на получение поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getPosts($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->getPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\EnsiProject\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPost

> \EnsiProject\PostsClient\Dto\PostResponse patchPost($id, $patch_post_request)

Запрос на обновление отдельных полей поста

Запрос на обновление отдельных полей поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_post_request = new \EnsiProject\PostsClient\Dto\PatchPostRequest(); // \EnsiProject\PostsClient\Dto\PatchPostRequest | 

try {
    $result = $apiInstance->patchPost($id, $patch_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->patchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_post_request** | [**\EnsiProject\PostsClient\Dto\PatchPostRequest**](../Model/PatchPostRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replacePost

> \EnsiProject\PostsClient\Dto\PostResponse replacePost($id, $replace_post_request)

Запрос на обновление поста

Запрос на обновление поста

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_post_request = new \EnsiProject\PostsClient\Dto\ReplacePostRequest(); // \EnsiProject\PostsClient\Dto\ReplacePostRequest | 

try {
    $result = $apiInstance->replacePost($id, $replace_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->replacePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_post_request** | [**\EnsiProject\PostsClient\Dto\ReplacePostRequest**](../Model/ReplacePostRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPosts

> \EnsiProject\PostsClient\Dto\SearchPostsResponse searchPosts($search_posts_request)

Поиск постов, удовлетворяющих фильтру

Поиск постов, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new EnsiProject\PostsClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \EnsiProject\PostsClient\Dto\SearchPostsRequest(); // \EnsiProject\PostsClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPosts($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->searchPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\EnsiProject\PostsClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\EnsiProject\PostsClient\Dto\SearchPostsResponse**](../Model/SearchPostsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

